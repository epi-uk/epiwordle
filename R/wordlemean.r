#' @title get_probabilities
#' @description Extract probability of each Wordle score from tweet text
#' @param tweet_text chr
#' @return A vector of probabilities
get_probabilities <- function(tweet_text) {
  myvec <- stringr::str_match_all(tweet_text, "[0-9]{1,2}\\%") %>%
    purrr::flatten_chr() %>%
    stringr::str_remove("%") %>%
    as.numeric()
  return(myvec / 100)
}

#' @title wgt_mean
#' @description Calculate weighted mean from tweet text
#' @param tweet_text chr
#' @return Vector of means
wgt_mean <- function(tweet_text) {
  pc_vec <- get_probabilities(tweet_text)
  sum(pc_vec * c(1:6, 10))
}

#' @title wgt_sd
#' @description Calculate weighted standard deviation from tweet text
#' @param tweet_text chr
#' @return Vector of sds
wgt_sd <- function(tweet_text) {
  pc_vec <- get_probabilities(tweet_text)
  themean <- wgt_mean(tweet_text)
  sqrt(sum(pc_vec * (c(1:6, 10) - themean)^2))
}

#' @title get_wordletweets
#' @description Retrieve tweets from Wordlestats timeline
#' @param n Number of tweets to retrieve
#' @return A tibble of tweets
get_wordletweets <- function(n) {
  rtweet::get_timelines("wordlestats", n = n)
}

#' @title Get Wordlestats
#' @description Retrieve mean and standard deviation of WordleStats results.
#' @param n How many of the most recent results to retrieve, Default: 10
#' @return A tibble of results
#' @examples
#' \dontrun{
#' if (interactive()) {
#'   get_wordlestats(n = 2)
#' }
#' }
#' @rdname get_wordlestats
#' @export
#' @importFrom rlang .data
get_wordlestats <- function(n = 10) {
  tmls <- get_wordletweets(n)
  tmls %>%
    dplyr::mutate(
      day = lubridate::date(.data$created_at) - 1,
      wordle_num = stringr::str_match(.data$text, "Wordle [0-9]{3}")[, 1]
    ) %>%
    dplyr::rowwise() %>%
    dplyr::mutate(
      wgt_mean = wgt_mean(.data$text),
      wgt_sd = wgt_sd(.data$text)
    ) %>%
    dplyr::select(.data$day, .data$wordle_num, .data$wgt_mean, .data$wgt_sd) %>%
    dplyr::arrange(.data$day)
}
