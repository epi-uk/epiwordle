#' @title get_secret
#' @description Get secrets from environment variables
#' @param secret chr
#' @return Secret value
get_secret <- function(secret) {
  val <- Sys.getenv(secret)
  if (identical(val, "")) {
    stop(paste(secret, "env var has not been set. Check your .Renviron."))
  }
  val
}

#' @title Initialise Wordle scraper
#' @description Initialise the connection with your Twitter app
#' @details Secrets to connect to the Twitter app will be pulled from your
#' environment variables. Ensure these are set in your .Renviron.
#' @examples
#' \dontrun{
#' if (interactive()) {
#'   epiwordle::initialise()
#' }
#' }
#' @rdname initialise
#' @export
initialise <- function() {
  # get secrets from environment variables
  tokens <- c("appname", "api_key", "api_secret_key", "access_token", "access_token_secret") %>%
    purrr::set_names() %>%
    purrr::map(~ get_secret(paste0("TWITBOT_", toupper(.x))))

  # create token
  rtweet::create_token(
    app = tokens$appname,
    consumer_key = tokens$api_key,
    consumer_secret = tokens$api_secret_key,
    access_token = tokens$access_token,
    access_secret = tokens$access_token_secret,
  )
}
