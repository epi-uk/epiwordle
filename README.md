
# epiwordle <img src='man/figures/logo.png' align="right" height="73" />

<!-- badges: start -->
<!-- badges: end -->

The goal of epiwordle is to return the mean and standard deviation of all users' Wordle scores for the day so we can scale our scores.

## Installation

### Set up your Twitter API access

1. Follow [this guide](https://cran.r-project.org/web/packages/rtweet/vignettes/auth.html) to obtain a Twitter developer account and create an app. You will need all four secrets described in that guide.
2. Add the four secrets to your [`.Renviron`](https://usethis.r-lib.org/reference/edit.html). It should look something like this (these are fake secrets, they won't work)

```text
TWITBOT_APPNAME=mywordlebot
TWITBOT_API_KEY=afYS4vbIlPAj096E60c4W1fiK
TWITBOT_API_SECRET_KEY=bI91kqnqFoNCrZFbsjAWHD4gJ91LQAhdCJXCj3yscfuULtNkuu
TWITBOT_ACCESS_TOKEN=9551451262-wK2EmA942kxZYIwa5LMKZoQA4Xc2uyIiEwu2YXL
TWITBOT_ACCESS_TOKEN_SECRET=9vpiSGKg1fIPQtxc5d5ESiFlZQpfbknEN1f1m2xe5byw7
```

### Install the EPI Wordle scraper

Install straight from GitLab

```r
devtools::install_gitlab('epi-uk/epiwordle')
```

## Example

Pull some stats:

``` r
library(epiwordle)

## initialise the scraper
epiwordle::initialise()

## get the latest ten sets of stats
epiwordle::get_wordlestats()
```
